/*******************************************************************************
 * Copyright (c) 2000, 2004 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials 
 * are made available under the terms of the Common Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/cpl-v10.html
 * 
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package org.eclipse.ui.internal;

/**
 * This class defines constants for looking up resources that are available
 * only within the Eclipse UI and Eclipse UI Standard Components projects.
 */
public interface IWorkbenchGraphicConstants {

	public final static String IMG_ETOOL_IMPORT_WIZ = "IMG_ETOOL_IMPORT_WIZ"; //$NON-NLS-1$
	public final static String IMG_ETOOL_IMPORT_WIZ_HOVER = "IMG_ETOOL_IMPORT_WIZ_HOVER"; //$NON-NLS-1$
	public final static String IMG_ETOOL_IMPORT_WIZ_DISABLED = "IMG_ETOOL_IMPORT_WIZ_DISABLED"; //$NON-NLS-1$

	public final static String IMG_ETOOL_EXPORT_WIZ = "IMG_ETOOL_EXPORT_WIZ"; //$NON-NLS-1$
	public final static String IMG_ETOOL_EXPORT_WIZ_HOVER = "IMG_ETOOL_EXPORT_WIZ_HOVER"; //$NON-NLS-1$
	public final static String IMG_ETOOL_EXPORT_WIZ_DISABLED = "IMG_ETOOL_EXPORT_WIZ_DISABLED"; //$NON-NLS-1$

	public final static String IMG_ETOOL_SAVE_EDIT = "IMG_ETOOL_SAVE_EDIT" ; //$NON-NLS-1$
	public final static String IMG_ETOOL_SAVE_EDIT_HOVER = "IMG_ETOOL_SAVE_EDIT_HOVER" ; //$NON-NLS-1$
	public final static String IMG_ETOOL_SAVE_EDIT_DISABLED = "IMG_ETOOL_SAVE_EDIT_DISABLED" ; //$NON-NLS-1$

	public final static String IMG_ETOOL_SAVEAS_EDIT = "IMG_ETOOL_SAVEAS_EDIT" ; //$NON-NLS-1$
	public final static String IMG_ETOOL_SAVEAS_EDIT_HOVER = "IMG_ETOOL_SAVEAS_EDIT_HOVER" ; //$NON-NLS-1$
	public final static String IMG_ETOOL_SAVEAS_EDIT_DISABLED = "IMG_ETOOL_SAVEAS_EDIT_DISABLED" ; //$NON-NLS-1$

	public final static String IMG_ETOOL_SAVEALL_EDIT = "IMG_ETOOL_SAVEALL_EDIT" ; //$NON-NLS-1$
	public final static String IMG_ETOOL_SAVEALL_EDIT_HOVER = "IMG_ETOOL_SAVEALL_EDIT_HOVER" ; //$NON-NLS-1$
	public final static String IMG_ETOOL_SAVEALL_EDIT_DISABLED = "IMG_ETOOL_SAVEALL_EDIT_DISABLED" ; //$NON-NLS-1$
	
	public final static String IMG_ETOOL_PRINT_EDIT = "IMG_ETOOL_PRINT_EDIT" ; //$NON-NLS-1$
	public final static String IMG_ETOOL_PRINT_EDIT_HOVER = "IMG_ETOOL_PRINT_EDIT_HOVER" ; //$NON-NLS-1$
	public final static String IMG_ETOOL_PRINT_EDIT_DISABLED = "IMG_ETOOL_PRINT_EDIT_DISABLED" ; //$NON-NLS-1$

	public final static String IMG_ETOOL_SEARCH_SRC = "IMG_ETOOL_SEARCH_SRC" ; //$NON-NLS-1$
	public final static String IMG_ETOOL_SEARCH_SRC_HOVER = "IMG_ETOOL_SEARCH_SRC_HOVER" ; //$NON-NLS-1$
	public final static String IMG_ETOOL_SEARCH_SRC_DISABLED = "IMG_ETOOL_SEARCH_SRC_DISABLED" ; //$NON-NLS-1$

	public final static String IMG_ETOOL_REFRESH_NAV = "IMG_ETOOL_REFRESH_NAV"; //$NON-NLS-1$
	public final static String IMG_ETOOL_REFRESH_NAV_HOVER = "IMG_ETOOL_REFRESH_NAV_HOVER"; //$NON-NLS-1$
	public final static String IMG_ETOOL_REFRESH_NAV_DISABLED = "IMG_ETOOL_REFRESH_NAV_DISABLED"; //$NON-NLS-1$

	public final static String IMG_ETOOL_HOME_NAV = "IMG_ETOOL_HOME_NAV"; //$NON-NLS-1$
	public final static String IMG_ETOOL_HOME_NAV_HOVER = "IMG_ETOOL_HOME_NAV_HOVER"; //$NON-NLS-1$
	public final static String IMG_ETOOL_HOME_NAV_DISABLED = "IMG_ETOOL_HOME_NAV_DISABLED"; //$NON-NLS-1$

	public final static String IMG_ETOOL_NEW_PAGE = "IMG_ETOOL_NEW_PAGE"; //$NON-NLS-1$
	public final static String IMG_ETOOL_NEW_PAGE_HOVER = "IMG_ETOOL_NEW_PAGE_HOVER"; //$NON-NLS-1$
	public final static String IMG_ETOOL_NEW_PAGE_DISABLED = "IMG_ETOOL_NEW_PAGE_DISABLED"; //$NON-NLS-1$

	public final static String IMG_ETOOL_PIN_EDITOR = "IMG_ETOOL_PIN_EDITOR"; //$NON-NLS-1$
	public final static String IMG_ETOOL_PIN_EDITOR_HOVER = "IMG_ETOOL_PIN_EDITOR_HOVER"; //$NON-NLS-1$
	public final static String IMG_ETOOL_PIN_EDITOR_DISABLED = "IMG_ETOOL_PIN_EDITOR_DISABLED"; //$NON-NLS-1$
		
	public final static String IMG_ETOOL_DEF_PERSPECTIVE = "IMG_ETOOL_DEF_PERSPECTIVE"; //$NON-NLS-1$
	public final static String IMG_ETOOL_DEF_PERSPECTIVE_HOVER = "IMG_ETOOL_DEF_PERSPECTIVE_HOVER"; //$NON-NLS-1$
	
	public final static String IMG_ETOOL_HELP_CONTENTS = "IMG_ETOOL_HELP_CONTENTS"; //$NON-NLS-1$
	
	// local toolbars
	public final static String IMG_LCL_CLOSE_VIEW = "IMG_LCL_CLOSE_VIEW" ; //$NON-NLS-1$
	public final static String IMG_LCL_CLOSE_VIEW_HOVER = "IMG_LCL_CLOSE_VIEW_HOVER"; //$NON-NLS-1$	
	public final static String IMG_LCL_PIN_VIEW = "IMG_LCL_PIN_VIEW" ; //$NON-NLS-1$
	public final static String IMG_LCL_PIN_VIEW_HOVER = "IMG_LCL_PIN_VIEW_HOVER"; //$NON-NLS-1$
	public final static String IMG_LCL_MIN_VIEW = "IMG_LCL_MIN_VIEW" ; //$NON-NLS-1$
	public final static String IMG_LCL_MIN_VIEW_HOVER = "IMG_LCL_MIN_VIEW_HOVER"; //$NON-NLS-1$
	public final static String IMG_LCL_VIEW_MENU = "IMG_LCL_VIEW_MENU"; //$NON-NLS-1$
	public final static String IMG_LCL_VIEW_MENU_HOVER = "IMG_LCL_VIEW_MENU_HOVER"; //$NON-NLS-1$
	public final static String IMG_LCL_SELECTED_MODE = "IMG_LCL_SELECTED_MODE"; //$NON-NLS-1$
	public final static String IMG_LCL_SHOWCHILD_MODE = "IMG_LCL_SHOWCHILD_MODE"; //$NON-NLS-1$
	public final static String IMG_LCL_TREE_MODE = "IMG_LCL_TREE_MODE"; //$NON-NLS-1$
	public final static String IMG_LCL_DEFAULTS_PS = "IMG_LCL_DEFAULTS_PS"; //$NON-NLS-1$
	public final static String IMG_LCL_FILTER_PS = "IMG_LCL_FILTER_PS"; //$NON-NLS-1$
	public final static String IMG_LCL_SHOWSYNC_RN = "IMG_LCL_SHOWSYNC_RN"; //$NON-NLS-1$
	public static final String IMG_LCL_LINKTO_HELP = "IMGS_LCL_LINKTO_HELP"; //$NON-NLS-1$

	//wizard images
	public final static String IMG_WIZBAN_NEW_WIZ = "IMG_WIZBAN_NEW_WIZ"; //$NON-NLS-1$
	public final static String IMG_WIZBAN_EXPORT_WIZ = "IMG_WIZBAN_EXPORT_WIZ"; //$NON-NLS-1$
	public final static String IMG_WIZBAN_IMPORT_WIZ = "IMG_WIZBAN_IMPORT_WIZ"; //$NON-NLS-1$
	public final static String IMG_WIZBAN_WORKINGSET_WIZ = "IMG_WIZBAN_WORKINGSET_WIZ"; //$NON-NLS-1$
	
	public final static String IMG_VIEW_DEFAULTVIEW_MISC = "IMG_VIEW_DEFAULTVIEW_MISC"; //$NON-NLS-1$
	
	// dialog images
	public final static String IMG_DLGBAN_SAVEAS_DLG = "IMG_DLGBAN_SAVEAS_DLG"; //$NON-NLS-1$

	// part direct manipulation objects
	public final static String IMG_OBJS_DND_LEFT_SOURCE = "IMG_OBJS_DND_LEFT_SOURCE"; //$NON-NLS-1$
	public final static String IMG_OBJS_DND_LEFT_MASK = "IMG_OBJS_DND_LEFT_MASK"; //$NON-NLS-1$
	public final static String IMG_OBJS_DND_RIGHT_SOURCE = "IMG_OBJS_DND_RIGHT_SOURCE"; //$NON-NLS-1$
	public final static String IMG_OBJS_DND_RIGHT_MASK = "IMG_OBJS_DND_RIGHT_MASK"; //$NON-NLS-1$
	public final static String IMG_OBJS_DND_TOP_SOURCE = "IMG_OBJS_DND_TOP_SOURCE"; //$NON-NLS-1$
	public final static String IMG_OBJS_DND_TOP_MASK = "IMG_OBJS_DND_TOP_MASK"; //$NON-NLS-1$
	public final static String IMG_OBJS_DND_BOTTOM_SOURCE = "IMG_OBJS_DND_BOTTOM_SOURCE"; //$NON-NLS-1$
	public final static String IMG_OBJS_DND_BOTTOM_MASK = "IMG_OBJS_DND_BOTTOM_MASK"; //$NON-NLS-1$
	public final static String IMG_OBJS_DND_INVALID_SOURCE = "IMG_OBJS_DND_INVALID_SOURCE"; //$NON-NLS-1$
	public final static String IMG_OBJS_DND_INVALID_MASK = "IMG_OBJS_DND_INVALID_MASK"; //$NON-NLS-1$
	public final static String IMG_OBJS_DND_STACK_SOURCE = "IMG_OBJS_DND_STACK_SOURCE"; //$NON-NLS-1$
	public final static String IMG_OBJS_DND_STACK_MASK = "IMG_OBJS_DND_STACK_MASK"; //$NON-NLS-1$
	public final static String IMG_OBJS_DND_OFFSCREEN_SOURCE = "IMG_OBJS_DND_OFFSCREEN_SOURCE"; //$NON-NLS-1$
	public final static String IMG_OBJS_DND_OFFSCREEN_MASK = "IMG_OBJS_DND_OFFSCREEN_MASK"; //$NON-NLS-1$
	public static final String IMG_OBJS_DND_TOFASTVIEW_SOURCE = "IMG_OBJS_DND_TOFASTVIEW_SOURCE"; //$NON-NLS-1$
	public static final String IMG_OBJS_DND_TOFASTVIEW_MASK = "IMG_OBJS_DND_TOFASTVIEW_MASK"; //$NON-NLS-1$    
    /**
     * Identifies an activity category.
     * 
     * @since 3.0
     */
    public static final String IMG_OBJ_ACTIVITY_CATEGORY = "IMG_OBJ_ACTIVITY_CATEGORY"; //$NON-NLS-1$
    /**
     * Identifies an activity.
     * 
     * @since 3.0
     */
    public static final String IMG_OBJ_ACTIVITY = "IMG_OBJ_ACTIVITY"; //$NON-NLS-1$

    /**
     * Identifies a font.
     * 
     * @since 3.0
     */
    public static final String IMG_OBJ_FONT = "IMG_OBJ_FONT"; //$NON-NLS-1$

    /**
     * Identifies a theme category.
     * 
     * @since 3.0
     */
    public static final String IMG_OBJ_THEME_CATEGORY = "IMG_OBJ_THEME_CATEGORY"; //$NON-NLS-1$    
}
