/*******************************************************************************
 * Copyright (c) 2000, 2003 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials 
 * are made available under the terms of the Common Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/cpl-v10.html
 * 
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package org.eclipse.ui.internal.dialogs;

import org.eclipse.jface.preference.*;
/**
 * This class is created to avoid mentioning preferences
 * in this context. Ideally, JFace preference classes should be
 * renamed into something more generic (for example,
 * 'TreeNavigationDialog').
 */

public class PropertyPageManager extends PreferenceManager {
/**
 * The constructor.
 */
public PropertyPageManager() {
	super();
}
}
